from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .serializers import UserSerializer, GroupSerializer
from rest_framework import generics
from .serializers import *
from .models import *
from .permissions import IsOwnerOrReadOnly
from rest_framework.permissions import IsAuthenticated, IsAdminUser


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class BrandsCreateView(generics.CreateAPIView):
    """
    Создать бренд. Разрешено только администратору.
    """
    serializer_class = BrandsListSerializer
    permission_classes = (IsAdminUser,)


class BrandsListView(generics.ListAPIView):
    """
    Показать все бренды. Разрешено только администратору.
    """
    serializer_class = BrandsListSerializer
    queryset = CarBrands.objects.all()
    permission_classes = (IsAdminUser,)


class BrandsRetrieveView(generics.RetrieveAPIView):
    """
    Найти бренд. Разрешено только администратору.
    """
    serializer_class = BrandsListSerializer
    queryset = CarBrands.objects.all()
    permission_classes = (IsAdminUser,)


class BrandsUpdateView(generics.UpdateAPIView):
    """
    Обновить бренд. Разрешено только администратору.
    """
    serializer_class = BrandsListSerializer
    queryset = CarBrands.objects.all()
    permission_classes = (IsAdminUser,)


class BrandsDeleteView(generics.DestroyAPIView):
    """
    Удалить бренд. Разрешено только администратору.
    """
    serializer_class = BrandsListSerializer
    queryset = CarBrands.objects.all()
    permission_classes = (IsAdminUser,)


class ModelsCreateView(generics.CreateAPIView):
    """
    Создать модель. Разрешено только администратору.
    """
    serializer_class = ModelsListSerializer
    permission_classes = (IsAdminUser,)


class ModelsListView(generics.ListAPIView):
    """
    Показать все модели. Разрешено только администратору.
    """
    serializer_class = ModelsListSerializer
    queryset = CarModels.objects.all()
    permission_classes = (IsAdminUser,)


class ModelsRetrieveView(generics.RetrieveAPIView):
    """
    Найти модель машины. Разрешено только администратору
    """
    serializer_class = ModelsListSerializer
    queryset = CarModels.objects.all()
    permission_classes = (IsAdminUser, )


class ModelsUpdateView(generics.UpdateAPIView):
    """
    Обновить модель машины. Разрешено только администратору
    """
    serializer_class = ModelsListSerializer
    queryset = CarModels.objects.all()
    permission_classes = (IsAdminUser, )


class ModelsDeleteView(generics.DestroyAPIView):
    """
    Удалить модель машины. Разрешено только администратору
    """
    serializer_class = ModelsListSerializer
    queryset = CarModels.objects.all()
    permission_classes = (IsAdminUser, )


# Create some trip
class TripsCreateView(generics.CreateAPIView):
    """
    Создать поездку. Разрешено только авторизованному пользователю.
    """
    serializer_class = TripsListSerializer
    permission_classes = (IsAuthenticated, )


class TripsListView(generics.ListAPIView):
    """
    Показать все поездки. Разрешено только администратору.
    """
    serializer_class = TripsListSerializer
    queryset = Trips.objects.all()
    permission_classes = (IsAdminUser, )


class MyTripsListView(generics.ListAPIView):
    """
    Показать поездки текущего пользователя. Разрешено только авторизованному пользователю.
    """
    serializer_class = TripsListSerializer
    queryset = Trips.objects.all()

    def get_queryset(self):
        """Return object for current authenticated user only"""
        return self.queryset.filter(user=self.request.user)


class TripsRetrieveView(generics.RetrieveAPIView):
    """
    Найти поездку. Разрешено только администратору.
    """
    serializer_class = TripsListSerializer
    queryset = Trips.objects.all()
    permission_classes = (IsAdminUser, )


class TripsUpdateView(generics.UpdateAPIView):
    """
    Обновить поездку. Разрешено администратору или создателю поездки
    """
    serializer_class = TripsListSerializer
    queryset = Trips.objects.all()
    permission_classes = (IsAdminUser, IsAuthenticated)


class TripsDeleteView(generics.DestroyAPIView):
    """
    Удалить поездку. Разрешено администратору или создателю поездки
    """
    serializer_class = TripsListSerializer
    queryset = Trips.objects.all()
    permission_classes = (IsAdminUser, IsAuthenticated)
