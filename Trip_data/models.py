from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()


class CarBrands(models.Model):
    car_brand = models.CharField(verbose_name='Марка машины', unique=True, max_length=15)

    def __str__(self):
        return self.car_brand


class CarModels(models.Model):
    car_model = models.CharField(verbose_name='Модель машины', unique=True, max_length=15)
    car_brand = models.ForeignKey(CarBrands, verbose_name='Марка машины', on_delete=models.CASCADE)
    consumption_in_city = models.PositiveIntegerField(verbose_name='Расход по городу на 100км, л', default=10)
    consumption_in_the_country = models.PositiveIntegerField(verbose_name='Расход по трассе на 100км, л', default=7)

    def __str__(self):
        return self.car_model


class Trips(models.Model):
    car_model = models.ForeignKey(CarModels, verbose_name='Модель машины', on_delete=models.CASCADE)
    load_weight = models.PositiveIntegerField(verbose_name='Нагружаемая масса, кг', null=True)
    car_mileage = models.PositiveIntegerField(verbose_name='Пробег машины, км', null=True)
    time_start = models.DateTimeField(verbose_name='Время выезда', default='2020-01-25 14:30:59')
    go_from = models.CharField(verbose_name='Откуда', max_length=64)
    go_to = models.CharField(verbose_name='Куда', max_length=64)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE)
    estimated_consumption = models.PositiveIntegerField(verbose_name='Предполагаемый расход бензина, л', default=0)
    real_consumption = models.PositiveIntegerField(verbose_name='Реальный расход бензина, л', default=0)

    def __str__(self):
        return f'{self.id} of {self.user.username}'
