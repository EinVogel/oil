from django.urls import path
from .views import *

app_name = 'Trip_data'
urlpatterns = [
    # Trips
    path('create/trips/', TripsCreateView.as_view()),
    path('all/trips/', TripsListView.as_view()),
    path('my/trips/', MyTripsListView.as_view()),
    path('retrieve/trips/<int:pk>/', TripsRetrieveView.as_view()),
    path('update/trips/<int:pk>/', TripsUpdateView.as_view()),
    path('delete/trips/<int:pk>/', TripsDeleteView.as_view()),
    # Car Brands
    path('create/brands/', BrandsCreateView.as_view()),
    path('all/brands/', BrandsListView.as_view()),
    path('retrieve/brands/<int:pk>/', BrandsRetrieveView.as_view()),
    path('update/brands/<int:pk>/', BrandsUpdateView.as_view()),
    path('delete/brands/<int:pk>/', BrandsDeleteView.as_view()),
    # Car Models
    path('create/models/', ModelsCreateView.as_view()),
    path('all/models/', ModelsListView.as_view()),
    path('retrieve/models/<int:pk>/', ModelsRetrieveView.as_view()),
    path('update/models/<int:pk>/', ModelsUpdateView.as_view()),
    path('delete/models/<int:pk>/', ModelsDeleteView.as_view()),
]
