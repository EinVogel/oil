from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import CarBrands, CarModels, Trips


# Serializer converts data to JSON
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class BrandsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarBrands
        fields = '__all__'


class ModelsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarModels
        fields = ('car_brand', 'car_model', 'consumption_in_city', 'consumption_in_the_country')


class TripsListSerializer(serializers.ModelSerializer):
    # When user enters to app, he can not choose another users. Each operation create a trip will be complete with this
    # user
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Trips
        fields = ('id', 'user', 'car_model', 'load_weight', 'car_mileage', 'time_start', 'go_from', 'go_to')
