from django.apps import AppConfig


class TripDataConfig(AppConfig):
    name = 'Trip_data'
