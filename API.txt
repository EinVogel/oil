# User registration
127.0.0.1:8000/api/v1/auth/users/
# Create token
127.0.0.1:8000/api/v1/auth_token/token/login/
# Create trip
127.0.0.1:8000/api/v1/trips/create/trips/
# Show all trips
127.0.0.1:8000/api/v1/trips/all/trips/
# Update, delete trips
127.0.0.1:8000/api/v1/trips/update/trips/<int:pk>/

# Create brand
127.0.0.1:8000/api/v1/trips/create/brands/
# Show all trips
127.0.0.1:8000/api/v1/trips/all/brands/
# Update, delete trips
127.0.0.1:8000/api/v1/trips/update/brands/<int:pk>/

# Create model
127.0.0.1:8000/api/v1/trips/create/models/
# Show all models
127.0.0.1:8000/api/v1/trips/all/models/
# Update, delete model
127.0.0.1:8000/api/v1/trips/update/models/<int:pk>/